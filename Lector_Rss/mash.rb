require 'rest-client'
require 'json'
require 'date'
require "colorize"
require "launchy"



class Mashable  
    attr_accessor :page,:hash
        def initialize 
            @page = "https://www.mashable.com/stories.json"
        end
    
        def load_page
          @resp = RestClient.get(@page)
            @hash = JSON.parse(@resp)
        end 
    
        def show_new
            @cont = 1
            @hash["new"].each do |index|
            puts "Noticia numero #{@cont}".red
            puts "Titulo : #{index["title"]}".blue
            puts "Autor: #{index["author"]}".blue
            puts "Direccion de la noticia : #{index["link"]}".blue
            @dir=index["link"]
            puts "Fecha de creacion".red
            @ref = index["post_date"]
            @time = DateTime.parse(@ref)
            puts Time.at(@time.to_time.to_i).strftime("%d/%m/%Y %I:%M %p").red
            opc= gets.chomp.to_i
            if opc == 1 
            
               Launchy.open (@dir)
            end
            puts ""
            system ("clear")
            @cont +=1
        
                     end
        end
        def show_rising
            @cont = 1
            @hash["rising"].each do |index|
            puts "Noticia numero #{@cont}".red
            puts "Titulo: #{index["title"]}".blue
            puts "Autor: #{index["author"]}".blue
            puts "Direccion de la noticia : #{index["link"]}".blue
            @dir=index["link"]
            puts "Fecha de creacion".red
            @ref = index["post_date"]
            @time = DateTime.parse(@ref)
            puts Time.at(@time.to_time.to_i).strftime("%d/%m/%Y %I:%M %p").red
            puts ""
            opc= gets.chomp.to_i
            if opc == 1 
            
                Launchy.open (@dir)
             end
             puts ""
             system ("clear")
            
            @cont +=1
             end
        end
        def show_hot
            @cont = 1
            @hash["hot"].each do |index|
            puts "Noticia numero #{@cont}".red
            puts "Titulo: #{index["title"]}".blue
            puts "Autor: #{index["author"]}".blue
            puts "Direccion de la noticia : #{index["link"]}".blue
            @dir=index["link"]
            puts "Fecha de creacion".red
            @ref = index["post_date"]
            @time = DateTime.parse(@ref)
            puts Time.at(@time.to_time.to_i).strftime("%d/%m/%Y %I:%M %p").red
            puts ""
            opc= gets.chomp.to_i
            if opc == 1 
            
                Launchy.open (@dir)
             end
             puts ""
             system ("clear")
            
            @cont +=1
            end
        end
        def cambio_new
            
            @a = []
           @a = @hash["new"].sort_by {|k,v| k["post_date"]}
           @cont = 1
                    @a.each do |index|
                        puts "Noticia numero #{@cont}".red
                        puts "Titulo: #{index["title"]}".blue
                        puts "Autor: #{index["author"]}".blue
                        puts "Direccion de la noticia : #{index["link"]}".blue
                        @dir=index["link"]
                        puts "Fecha de creacion".red
                        @ref = index["post_date"]
                        @time = DateTime.parse(@ref)
                        puts Time.at(@time.to_time.to_i).strftime("%d/%m/%Y %I:%M %p").red
                        puts ""
                        opc= gets.chomp.to_i
                        if opc == 1 
            
                            Launchy.open (@dir)
                         end
                         puts ""
                         system ("clear")
                        @cont +=1
                        
                end
         end

         def cambio_hot
            
            @a = []
           @a = @hash["hot"].sort_by {|k,v| k["post_date"]}
           @cont = 1
                    @a.each do |index|
                        puts "Noticia numero #{@cont}".red
                        puts "Titulo: #{index["title"]}".blue
                        puts "Autor: #{index["author"]}".blue
                        puts "Direccion de la noticia : #{index["link"]}".blue
                        @dir=index["link"]
                        puts "Fecha de creacion".red
                        @ref = index["post_date"]
                        @time = DateTime.parse(@ref)
                        puts Time.at(@time.to_time.to_i).strftime("%d/%m/%Y %I:%M %p").red
                        puts ""
                        opc= gets.chomp.to_i
                        if opc == 1 
            
                            Launchy.open (@dir)
                         end
                         puts ""
                         system ("clear")
                        @cont +=1
                        
                end
         end
         def cambio_rising
            
            @a = []
           @a = @hash["rising"].sort_by {|k,v| k["post_date"]}
                    @cont = 1
                         @a.each do |index|
                        
                        puts "Noticia numero #{@cont}".red
                        puts "Titulo: #{index["title"]}".blue
                        puts "Autor: #{index["author"]}".blue
                        puts "Direccion de la noticia : #{index["link"]}".blue
                        @dir=index["link"]
                        puts "Fecha de creacion".red
                        @ref = index["post_date"]
                        @time = DateTime.parse(@ref)
                        puts Time.at(@time.to_time.to_i).strftime("%d/%m/%Y %I:%M %p").red
                        puts ""
                        opc= gets.chomp.to_i
                        if opc == 1 
            
                            Launchy.open (@dir)
                         end
                         puts ""
                         system ("clear")
                        @cont +=1
                        
                end
         end
         def cambio_todo
            
            @cont = 1
            @a = @hash["new"]
            @b = @hash["hot"]
            @c = @hash["rising"]
            @d = (@a + @b + @c ).sort_by {|k,v| k["post_date"]}
           
              @d.each do |index|
              
              puts "Noticia numero #{@cont}".red
              puts "Titulo: #{index["title"]}".blue
              puts "Autor: #{index["author"]}".blue
              puts "Direccion de la noticia : #{index["link"]}".blue
              @dir=index["link"]
              puts "Fecha de creacion".red
              @ref = index["post_date"]
              @time = DateTime.parse(@ref)
              puts Time.at(@time.to_time.to_i).strftime("%d/%m/%Y %I:%M %p").red
              puts ""
              opc= gets.chomp.to_i
              if opc == 1 
            
                Launchy.open (@dir)
             end
             puts ""
             system ("clear")
              @cont +=1
              end
            end
        
 end
 
    